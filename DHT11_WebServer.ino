/*
   Nama     : Mohamad Ardi
   Kelas    : IoT-4-D
   Project  : DHT 11 WebServer
   Date     : 02 Mei 2020
*/
#include <WiFi.h>
#include <WebServer.h>

#include "DHT.h"
#define DHTPIN 26
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

const char* ssid ="Wifi Orang"; 
const char* password ="Islam1000";

WebServer server(80);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  delay(100);
  dht.begin();

  Serial.print("Terhubung dengan ");
  Serial.println(ssid);

  WiFi.begin(ssid,password);   //Fungsi wifi.begin dibutuhkan untuk join dengan jaringan WIFI.

  while (WiFi.status() != WL_CONNECTED){  //Upaya untuk terhubung ke Internet.
    delay(1000);
    Serial.print("."); 
    }

    Serial.println("");
    Serial.println("WiFi Terhubung...!");
    Serial.print("IP = ");
    Serial.println(WiFi.localIP());   //Print alamat local IP

    server.on("/", handle_OnConnect);
    server.onNotFound(handle_NotFound);
    server.begin();
    Serial.println("HTTP server dimulai");
}

void loop() {
  // put your main code here, to run repeatedly:
  server.handleClient();
}

void handle_OnConnect(){
  float Suhu = dht.readTemperature();
  float Kelembapan = dht.readHumidity();
  server.send(200, "text/html", SendHTML(Suhu,Kelembapan));
}

 void handle_NotFound(){
  server.send(404, "text/plain", "Tidak Ditemukan");
}


String SendHTML(float Temperaturestat, float Humiditystat){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>Laporan Suhu & Kelembapan</title>\n";
  ptr +="<style>html {font-family: Monotype Corsiva; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #800000;margin: 50px auto 30px;}\n";
  ptr +="p {font-size: 24px;color: #800000;margin-bottom: 10px;}\n";
  ptr +="</style>\n";

  ptr +="<script>\n";
  ptr +="setInterval(loadDoc, 200);\n";
  ptr +="function loadDoc() {\n";
  ptr +="var xhttp = new XMLHttpRequest();\n";
  ptr +="xhttp.onreadystatechange = function() {\n";
  ptr +="if (this.readyState == 4 && this.status == 200) {\n";
  ptr +="document.getElementById(\"webpage\").innerHTML =this.responseText} \n";
  ptr +="};\n";
  ptr +="xhttp.open(\"GET\", \"/\", true);\n";
  ptr +="xhttp.send();\n";
  ptr +="}\n";
  ptr +="</script>\n";

  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<div id=\"webpage\">\n";
  ptr +="<h1>Laporan Suhu dan Kelembapan</h1>\n";

  ptr +="<p>Suhu       = ";
  ptr +=(int)Temperaturestat;
  ptr +="°C</p>";
  ptr +="<p>Kelembapan = ";
  ptr +=(int)Humiditystat;
  ptr +="%</p>";

  ptr +="</div>\n";
  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}  
